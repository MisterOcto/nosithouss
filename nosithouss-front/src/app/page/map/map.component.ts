import {
  AfterViewInit,
  ApplicationRef,
  Component,
  ComponentFactoryResolver, //todo deprecié à partir de Angular 9 à remplacer
  EmbeddedViewRef,
  Injector,
  ViewContainerRef,


} from '@angular/core';
import * as L from 'leaflet';
import {PostService} from "../../_service/post.service";
import {IPostGet} from "../../_interfaces/post";
import {MapMarkerPopupComponent} from "../../tools/map-marker-popup/map-marker-popup.component";

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  private map: null | L.Map = null;
  private userLat: number = 47.2138327;
  private userLon: number = -1.5456324;
  private posts: IPostGet[] = [];

  constructor(
    private viewContainerRef: ViewContainerRef,
    private postService: PostService,
    private appRef: ApplicationRef,
    private injector: Injector,
    private resolver: ComponentFactoryResolver
  ) {
  }

  ngAfterViewInit(): void {

    this.postService.getPostByType("GUARDING").subscribe(
      d => {
        this.posts = d
        this.initMap()
      }
    )
  }

  private initMap(): void {
    this.getLocation();
    this.map = L.map('map', {
      center: [this.userLat, this.userLon],
      zoom: 13
    });
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

    this.addMarkers();
  }


  private addMarkers() {
    this.posts.map(p => {
      const popupContent = this.createPopupContent(p);
      L.marker([p.coordinateY!, p.coordinateX!]).addTo(this.map!).bindPopup(popupContent);
    })
  }

  private createPopupContent(post: IPostGet): string {
    const factory = this.resolver.resolveComponentFactory(MapMarkerPopupComponent);
    const componentRef = this.viewContainerRef.createComponent(factory);
    componentRef.instance.post = post;
    this.appRef.attachView(componentRef.hostView);
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    return domElem.outerHTML;
  }

  private redirectToUser() {
    console.log("test")
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.userLat = position.coords.latitude;
          this.userLon = position.coords.longitude;
        },
        (error) => {
          console.error('Error getting location:', error);
        }
      );
    } else {
      console.error('Geolocation is not supported by this browser.');
    }
  }
}

