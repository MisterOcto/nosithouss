import { Component } from '@angular/core';
import { ICredential } from 'src/app/_interfaces/credential';
import { AuthService } from 'src/app/_service/auth.service';
import { TokenService } from 'src/app/_service/token.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {
  form: ICredential = {
    userName: '',
    password: '',
    acceptsCgu: false
  }

  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private router: Router) { }

  onSubmit(): void{
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenService.saveToken(data.bearer)
      },
      err => console.error(err)
    )
  }
  navigateToSignIn() {
    this.router.navigate(['/login']).then(r => console.log("navigateToLogin", r));
  }

  navigateToHome() {
    this.router.navigate(['/home']).then(r => console.log("navigateToHome", r));
  }

  navigateToCgu() {
    this.router.navigate(['/cgu']).then(r => console.log("navigateToCgu", r));
  }
}
