package fr.simsim.nosithoussauth.dtos.requests;

public record AuthenticationReq(String userName, String password) {

}
