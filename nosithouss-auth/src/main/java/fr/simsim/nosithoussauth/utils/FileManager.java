package fr.simsim.nosithoussauth.utils;


import fr.simsim.nosithoussauth.errors.NosithoussException;
import fr.simsim.nosithoussauth.repositories.ImageRepository;
import fr.simsim.nosithoussauth.models.Image;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@Component
public class FileManager {
    //    private static String uploadPath = "uploads";
    private final ImageRepository imageRepository;

    public FileManager(ImageRepository imageRepository) {this.imageRepository = imageRepository;}

    public byte[] getImage(String imageUUID) {
        try {
            return imageRepository.findByName(imageUUID).getData();
        } catch (Exception e) {
            throw new NosithoussException("Image not found: " + e.getMessage());
        }
    }

    public Image saveImage(MultipartFile file) throws IOException {
        String uniqueFileName = UUID.randomUUID().toString();
        return imageRepository.save(Image.builder().data(file.getBytes()).name(uniqueFileName).build());
    }

    public Image saveImage(byte[] file) {
        String uniqueFileName = UUID.randomUUID().toString();
        return imageRepository.save(Image.builder().data(file).name(uniqueFileName).build());
    }
}
