INSERT INTO public.comments (id, content, created_at, author_id, post_id)
VALUES (-1, 'Super intéressant ! 🌹✨ J''adore apprendre des détails sur les roses. Savais-tu que certaines variétés ont même un parfum délicieux ? 🌷😊 #PassionBotanique #Roses
', '2024-02-11 22:09:52.000000', 'simsim', -1);

INSERT INTO public.comments (id, content, created_at, author_id, post_id)
VALUES (-2,
        'Salut la team green thumbs ! 🌿 Ah, la vie des plantes pendant nos escapades ! 😅 Pour assurer le bien-être de ta précieuse verdure, tu peux préparer une petite routine avant ton départ. Assure-toi qu''elle ait suffisamment d''eau avant de partir et place-la dans un endroit avec une luminosité adéquate. Pour une astuce supplémentaire, demander à un voisin ou un ami de jeter un coup d''œil de temps en temps. Bonne vadrouille et que tes plantes restent en pleine forme ! 🌱✨🚗💚'
           , '2024-02-11 22:09:52.000000', 'simsim', -2);

INSERT INTO public.comments (id, content, created_at, author_id, post_id)
VALUES (-3,
        'Salut ! 🌿 Ne t''inquiète pas trop, j''ai quelques astuces pour toi ! Assure-toi de placer ton bonsaï dans un endroit lumineux, mais à l''abri de la lumière directe du soleil. Arrose-le bien avant ton départ et envisage un système d''arrosage automatique si possible. Place-le également dans un endroit moins exposé aux fluctuations de température. Bonne vadrouille et bon courage à ton bonsaï ! 🚗💚✨'
           , '2024-02-11 22:09:52.000000', 'nolwax', -5);

INSERT INTO public.comments (id, content, created_at, author_id, post_id)
VALUES (-4,
        'Salut ! 🌿 Pas de panique, arrose bien ton bonsaï avant de partir et place-le dans un coin lumineux, mais à l''ombre directe. Il devrait bien se débrouiller pendant ton absence ! Bon voyage ! 🚗🌱✨'
           , '2024-02-11 22:09:52.000000', 'oussouss', -5);

INSERT INTO public.comments (id, content, created_at, author_id, post_id)
VALUES (-5,
        'Haha, ton bonsaï doit être en mode "FOMO" là ! 😄 Laisse-lui une petite playlist de musique apaisante, et dis-lui de ne pas trop faire la fête pendant que tu seras parti. 🎶🌿 Bonnes aventures à ton mini-arbre ! 🚀✨'
           , '2024-02-11 22:09:52.000000', 'oussouss', -5);