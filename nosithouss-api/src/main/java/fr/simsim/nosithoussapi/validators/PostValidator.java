package fr.simsim.nosithoussapi.validators;

import fr.simsim.nosithoussapi.enums.EPostType;

public class PostValidator {
    public static boolean   isValidType(String type) {
        return type != null
                && EPostType.fromString(type) != null;
    }
}
