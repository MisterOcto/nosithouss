package fr.simsim.nosithoussapi.controllers;

import fr.simsim.nosithoussapi.dtos.requests.MessageReq;
import fr.simsim.nosithoussapi.dtos.responses.ContactRes;
import fr.simsim.nosithoussapi.dtos.responses.MessageRes;
import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.models.User;
import fr.simsim.nosithoussapi.services.ContactService;
import fr.simsim.nosithoussapi.services.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contact")
public class ContactController {

    private ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping(value = "")
    @Operation(summary = "Get all the user you send a message or who have sent you message, base on your token")
    public ResponseEntity getAllContact() {
        return ResponseEntity.ok(contactService.getAllContact());
    }
}
