package fr.simsim.nosithoussapi.controllers;

import fr.simsim.nosithoussapi.dtos.requests.AuthenticationReq;
import fr.simsim.nosithoussapi.dtos.responses.AuthLoginRes;
import fr.simsim.nosithoussapi.models.User;
import fr.simsim.nosithoussapi.services.JwtService;
import fr.simsim.nosithoussapi.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/auth")
public class AuthController {
    private AuthenticationManager authenticationManager;
    private UserService userService;
    private JwtService jwtService;


    @PostMapping("/register")
    @Operation(summary = "Register a new account")
    public void register(@RequestBody User user) {
        log.info("[NOSITHOUS] [auth] Post api/auth/register");
        userService.register(user);
    }


    @PostMapping("/login")
    @Operation(summary = "Login to your account, send security jwt token if login successfully")
    public AuthLoginRes login(@RequestBody AuthenticationReq authenticationReq) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationReq.userName(), authenticationReq.password()));

        if (authenticate.isAuthenticated()) {
            Map<String, String> jwt = jwtService.generate(authenticationReq.userName());
            return new AuthLoginRes(jwt.get("bearer"), authenticationReq.userName());
        }
        throw new RuntimeException("Authentication failed");
    }
}
