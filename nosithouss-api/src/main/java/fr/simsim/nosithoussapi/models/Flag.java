package fr.simsim.nosithoussapi.models;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "flags")
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Flag {
    @Id
    @Column(name = "_key")
    private String key;
    @Column(name = "_value")
    private String value;
    @Column(name = "_date")
    private Timestamp date;
}


