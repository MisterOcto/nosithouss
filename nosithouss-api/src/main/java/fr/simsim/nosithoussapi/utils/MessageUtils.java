package fr.simsim.nosithoussapi.utils;

import fr.simsim.nosithoussapi.dtos.responses.CatalogPostRes;
import fr.simsim.nosithoussapi.dtos.responses.GuardingPostRes;
import fr.simsim.nosithoussapi.dtos.responses.PostRes;
import fr.simsim.nosithoussapi.enums.EMessageType;
import fr.simsim.nosithoussapi.enums.EPostType;
import fr.simsim.nosithoussapi.models.*;

import javax.sound.midi.MetaMessage;
import java.util.List;

public class MessageUtils {

    public static EMessageType getEMessageType(Message message) {
        return switch (message) {
            case MessageRequestGuard messageRequestGuard -> EMessageType.GUARD_CLAIM;
            default -> EMessageType.MESSAGE;
        };
    }
}
