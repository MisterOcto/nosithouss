package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.models.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserNameRes {

    private String userName;
    private String pdp;
}
