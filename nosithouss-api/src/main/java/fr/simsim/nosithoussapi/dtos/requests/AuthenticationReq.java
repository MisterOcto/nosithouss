package fr.simsim.nosithoussapi.dtos.requests;

public record AuthenticationReq(String userName, String password) {

}
