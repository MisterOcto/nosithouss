package fr.simsim.nosithoussapi.dtos.requests;

import fr.simsim.nosithoussapi.enums.EMessageType;
import fr.simsim.nosithoussapi.models.Message;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageGuardReq extends MessageReq {
    private EMessageType type;
    private Boolean accept;
    private Long postId;
}
