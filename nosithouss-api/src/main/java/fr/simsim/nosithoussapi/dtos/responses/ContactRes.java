package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.models.Contact;
import fr.simsim.nosithoussapi.models.User;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContactRes {
    private String userName;
    private Date lastChat;
}
